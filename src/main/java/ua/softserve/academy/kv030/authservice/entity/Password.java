package ua.softserve.academy.kv030.authservice.entity;

import java.math.BigDecimal;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.*;

@Entity
@Table(name = "PASSWORDS")
public class Password {

	@Id
	@Column(name = "PASSWORD_ID", unique = true, nullable = false)
	@GeneratedValue
	private BigDecimal passwordId;
	
	@Column(name = "PASS", nullable = false, length = 256)
	private String password;
	
	@Column(name = "EXPIRATION_TIME", nullable = false)
	private Timestamp expirationTime;
	
	@Column(name = "STATUS", nullable = false)
	private boolean status;

	public Password() {
	}

	public Password(BigDecimal passwordId, Timestamp expirationTime, boolean status) {
		this.passwordId = passwordId;
		this.expirationTime = expirationTime;
		this.status = status;
	}

	public BigDecimal getPasswordId() {
		return passwordId;
	}

	public void setPasswordId(BigDecimal passwordId) {
		this.passwordId = passwordId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Timestamp getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Timestamp expirationTime) {
		this.expirationTime = expirationTime;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
}
