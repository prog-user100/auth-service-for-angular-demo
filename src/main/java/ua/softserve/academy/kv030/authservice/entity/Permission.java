package ua.softserve.academy.kv030.authservice.entity;

import javax.persistence.*;

@Entity
@Table(name = "PERMISSIONS")
public class Permission {
	
	@Id
	@GeneratedValue
	@Column(name = "PERMISSION_ID")
	private int permissionId;
	
	@Column(name = "PERMISSION_NAME")
	private String permission;

	public Permission() {
	}

	public Permission(int permissionId, String permission) {
		this.permissionId = permissionId;
		this.permission = permission;
	}

	public int getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

}
