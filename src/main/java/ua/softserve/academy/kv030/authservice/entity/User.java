package ua.softserve.academy.kv030.authservice.entity;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "USERS")
public class User {

	@Id
	@Column(name = "USER_ID", unique = true, nullable = false)
	private BigDecimal userId;

	@ManyToOne
	@JoinColumn(name = "ROLE_ID")
	private Role role;

	@Column(name = "EMAIL", nullable = false, length = 128)
	private String email;

	@OneToOne
	@PrimaryKeyJoinColumn(name = "PASSWORD_ID")
	private Password password;

	@Column(name = "USER_NAME", nullable = false, length = 45)
	private String name;

	@Column(name = "STATUS", nullable = false)
	private boolean status;

	@ManyToMany
	@JoinTable(name = "USER_FILE_ACCESS", joinColumns = { @JoinColumn(name = "USER_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "RESOURCE_ID") })
	private Set<Resource> resources;

	public User(){
	}

	public BigDecimal getUserId() {
		return userId;
	}

	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Password getPassword() {
		return password;
	}

	public void setPassword(Password password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Set<Resource> getResources() {
		return resources;
	}

	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}

}
