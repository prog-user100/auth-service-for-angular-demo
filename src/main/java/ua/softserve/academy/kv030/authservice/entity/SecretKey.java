package ua.softserve.academy.kv030.authservice.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "SECRET_KEY")
public class SecretKey {

	@Id
	@GeneratedValue
	@Column(name = "KEY_ID")
	private BigDecimal keyId;

	@Column(name = "EXPIRATION_DATE")
	private Timestamp expirationDate;

	@Column(name = "STATUS")
	private boolean status;

	@Column(name = "KEY_VALUE")
	private String key;

	public SecretKey() {
	}

	public SecretKey(BigDecimal keyId, Timestamp expirationDate, boolean status, String key) {
		this.keyId = keyId;
		this.expirationDate = expirationDate;
		this.status = status;
		this.key = key;
	}

	public BigDecimal getKeyId() {
		return keyId;
	}

	public void setKeyId(BigDecimal keyId) {
		this.keyId = keyId;
	}

	public Timestamp getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Timestamp expirationDate) {
		this.expirationDate = expirationDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}
