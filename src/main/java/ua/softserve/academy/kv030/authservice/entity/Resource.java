package ua.softserve.academy.kv030.authservice.entity;

import java.math.BigDecimal;
import javax.persistence.*;

@Entity
@Table(name = "RESOURCE")
public class Resource {

	@Id
	@GeneratedValue
	@Column(name = "RESOURCE_ID")
	private BigDecimal resourceId;

	@ManyToOne
	@JoinColumn(name = "KEY_ID")
	private SecretKey secretKey;

	@Column(name = "LINK_TO_FILE", nullable = false)
	private String linkToFile;

	@ManyToOne
	@JoinColumn(name = "PERMISSION_ID")
	private Permission persmission;

	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private User user;

	public Resource() {
	}

	public Resource(BigDecimal resourceId, SecretKey secretKey, String linkToFile, Permission persmission, User user) {
		this.resourceId = resourceId;
		this.secretKey = secretKey;
		this.linkToFile = linkToFile;
		this.persmission = persmission;
		this.user = user;
	}

	public BigDecimal getResourceId() {
		return resourceId;
	}

	public void setResourceId(BigDecimal resourceId) {
		this.resourceId = resourceId;
	}

	public SecretKey getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(SecretKey secretKey) {
		this.secretKey = secretKey;
	}

	public String getLinkToFile() {
		return linkToFile;
	}

	public void setLinkToFile(String linkToFile) {
		this.linkToFile = linkToFile;
	}

	public Permission getPersmission() {
		return persmission;
	}

	public void setPersmission(Permission persmission) {
		this.persmission = persmission;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
