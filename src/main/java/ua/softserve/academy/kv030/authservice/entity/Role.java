package ua.softserve.academy.kv030.authservice.entity;

import javax.persistence.*;

@Entity
@Table(name = "ROLES")
public class Role {

	@Column(name = "ROLE_ID", nullable = false)
	private int roleId;

	@Column(name = "ROLE_NAME", nullable = false, length = 45)
	private String roleName;

	public Role(){
	}
	
	public Role(int roleId, String roleName) {
		this.roleId = roleId;
		this.roleName = roleName;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
